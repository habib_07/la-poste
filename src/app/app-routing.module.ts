import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: './home/home.module#HomePageModule'
  },
  {
    path: 'list',
    loadChildren: './list/list.module#ListPageModule'
  },
  { path: 'localiser', loadChildren: './mesPages/localiser/localiser.module#LocaliserPageModule' },
  { path: 'telechargements', loadChildren: './mesPages/telechargements/telechargements.module#TelechargementsPageModule' },
  { path: 'connexion', loadChildren: './mesPages/connexion/connexion.module#ConnexionPageModule' },
  { path: 'trouver-cp', loadChildren: './mesPages/trouver-cp/trouver-cp.module#TrouverCPPageModule' },
  { path: 'details-cp/:id', loadChildren: './mesPages/details-cp/details-cp.module#DetailsCPPageModule' },
  { path: 'historique-transactions', loadChildren: './mesPages/compte/historique-transactions/historique-transactions.module#HistoriqueTransactionsPageModule' },
  { path: 'solde', loadChildren: './mesPages/compte/solde/solde.module#SoldePageModule' },
  { path: 'a-propos', loadChildren: './mesPages/a-propos/a-propos.module#AProposPageModule' },
  { path: 'contact', loadChildren: './mesPages/contact/contact.module#ContactPageModule' },
  { path: 'acceuil-client', loadChildren: './mesPages/compte/acceuil-client/acceuil-client.module#AcceuilClientPageModule' },
  { path: 'paiement-facture', loadChildren: './mesPages/paiement-facture/paiement-facture.module#PaiementFacturePageModule' },
  { path: 'suivi', loadChildren: './mesPages/suivi/suivi.module#SuiviPageModule' },
  { path: 'cours-de-change', loadChildren: './mesPages/cours-de-change/cours-de-change.module#CoursDeChangePageModule' },
  { path: 'soldes', loadChildren: './mesPages/soldes/soldes.module#SoldesPageModule' },
  { path: 'details-bp/:id', loadChildren: './mesPages/details-bp/details-bp.module#DetailsBPPageModule' },
  { path: 'monespace', loadChildren: './mesPages/monespace/monespace.module#MonespacePageModule' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
