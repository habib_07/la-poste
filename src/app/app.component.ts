import { Component } from '@angular/core';

import {Events, Platform} from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
showuser=false
  username
  appPages:any=[]

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public events: Events
  ) {
    events.subscribe('state', (user) => {
      console.log('Welcome', user)
      if(user=="connected"){
        this.appPages=this.appPages2
        this.showuser=true
      }else {
        this.appPages=this.appPages1
        this.showuser=false
      }
    });
    events.subscribe('user_key', (user) => {
      console.log('userkey', user)
      this.username=user
    });
    this.appPages=this.appPages1

    this.initializeApp();





  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.backgroundColorByHexString("white")
      this.splashScreen.hide();
    });
  }
  public appPages1 = [
    {
      title: 'Acceuil',
      url: '/home',
      icon: 'home',

    },
    {
      title: 'Se connecter',
      url: '/connexion',
      icon: 'log-in',

    },


    {
      title: 'Localiser un Bureau/DAB',
      url: '/localiser',
      icon: 'locate',

    },
    {
      title: 'Trouver un code postal ',
      url: '/trouver-cp',
      icon: 'search',

    },
    {
      title: 'Suivre votre colis',
      url: '/suivi',
      icon: 'cube',

    },
    {
      title: 'Les cours de change',
      url: '/cours-de-change',
      icon: 'logo-usd',

    },
    {
      title: 'Liens utiles',
      url: '/telechargements',
      icon: 'download',

    },
    {
      title: 'À propos',
      url: '/a-propos',
      icon: 'information-circle-outline',

    },
    {
      title: 'Contactez-nous',
      url: '/contact',
      icon: 'paper-plane',

    },
  ];
  public appPages2 = [
    {
      title: 'Acceuil',
      url: '/home',
      icon: 'home',

    },
    {
      title: 'Mon espace',
      url: '/monespace',
      icon: 'person',

    },
    {
      title: 'Mes transactions',
      url: '/soldes',
      icon: 'swap',

    },
    {
      title: 'Localiser un Bureau/DAB',
      url: '/localiser',
      icon: 'locate',

    },
    {
      title: 'Trouver un code postal ',
      url: '/trouver-cp',
      icon: 'search',

    },
    {
      title: 'Suivre votre colis',
      url: '/suivi',
      icon: 'cube',

    },
    {
      title: 'Les cours de change',
      url: '/cours-de-change',
      icon: 'logo-usd',

    },
    {
      title: 'Liens utiles',
      url: '/telechargements',
      icon: 'download',

    },
    {
      title: 'À propos',
      url: '/a-propos',
      icon: 'information-circle-outline',

    },
    {
      title: 'Contactez-nous',
      url: '/contact',
      icon: 'paper-plane',

    },


  ];
  logout(){
    console.log("im in logout")
    this.events.publish('state','desconnected', Date.now());
    localStorage.clear()

  }


}
