import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'bPS'
})
export class BPSPipe implements PipeTransform {

  transform(value: any, term: any): any {

    if(term===undefined){
      return value

    }else {
      console.log('im in')
      return value.filter(res=>res.bureau.toLowerCase().includes(term.toLowerCase())||res['code-postal'].toString().includes(term))

    }
  }

}
