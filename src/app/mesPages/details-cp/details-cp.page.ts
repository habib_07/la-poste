import { Component, OnInit } from '@angular/core';
import { BureauxDePoste, TrouverCPService } from 'src/app/mesServices/trouver-cp.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-details-cp',
  templateUrl: './details-cp.page.html',
  styleUrls: ['./details-cp.page.scss'],
})
export class DetailsCPPage implements OnInit {

  poste: BureauxDePoste
id;
  constructor(private activatedRoute:ActivatedRoute,private posteService:TrouverCPService) { }

  ngOnInit() {
    this.id=this.activatedRoute.snapshot.paramMap.get('id')
  }
 
  ionViewWillEnter(){
    if(this.id){
      this.posteService.getPoste(this.id).subscribe(poste =>{
        this.poste=poste;
      });}}}
