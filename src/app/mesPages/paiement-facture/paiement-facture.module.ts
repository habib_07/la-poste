import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PaiementFacturePage } from './paiement-facture.page';

const routes: Routes = [
  {
    path: '',
    component: PaiementFacturePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PaiementFacturePage]
})
export class PaiementFacturePageModule {}
