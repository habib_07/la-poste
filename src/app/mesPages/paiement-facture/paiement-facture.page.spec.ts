import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaiementFacturePage } from './paiement-facture.page';

describe('PaiementFacturePage', () => {
  let component: PaiementFacturePage;
  let fixture: ComponentFixture<PaiementFacturePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaiementFacturePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaiementFacturePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
