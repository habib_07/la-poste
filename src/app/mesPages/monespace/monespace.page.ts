import { Component, OnInit } from '@angular/core';
import {AngularFireDatabase} from '@angular/fire/database';
import { AngularFireStorage } from 'angularfire2/storage';

@Component({
  selector: 'app-monespace',
  templateUrl: './monespace.page.html',
  styleUrls: ['./monespace.page.scss'],
})
export class MonespacePage implements OnInit {
userkey=localStorage.getItem('userkey')
  user
  filesToUploads
  downloadurl
  indice
  constructor(private fDatabase: AngularFireDatabase,private fbstorage:AngularFireStorage) {
    this.moi()
  }

  ngOnInit() {

  }

  moi() {
    this.fDatabase.list('/clients/info-client',ref => ref.orderByChild("num_carte").equalTo(Number (this.userkey))).valueChanges().subscribe(res=>{

      this.user=res[0]
      console.log(this.user)
      console.log(this.user['picture'])
      this.indice=this.user['indice']

    })
  }
  fileChangeEvent(fileInput:any) {
    this.filesToUploads = <Array<File>>fileInput.target.files;
    console.log(this.filesToUploads[0]);
    this.upload2()
  }

  upload2(){
    this.fbstorage.upload('/user_pic/'+this.filesToUploads[0].name,this.filesToUploads[0]).then(res=> {


      this.fbstorage.ref('/user_pic/' + this.filesToUploads[0].name).getDownloadURL().toPromise().then(res=>{

        this.downloadurl=res
        console.log('url',this.downloadurl)

this.fDatabase.object("/clients/info-client/"+this.indice).update({
  picture:this.downloadurl
}).then(result=>{
  this.moi()
})



      })

    })

}
  }
