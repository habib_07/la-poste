import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonespacePage } from './monespace.page';

describe('MonespacePage', () => {
  let component: MonespacePage;
  let fixture: ComponentFixture<MonespacePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonespacePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonespacePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
