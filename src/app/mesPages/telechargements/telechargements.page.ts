import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import {AngularFireDatabase} from "angularfire2/database";

@Component({
  selector: 'app-telechargements',
  templateUrl: './telechargements.page.html',
  styleUrls: ['./telechargements.page.scss'],
})
export class TelechargementsPage implements OnInit {
listL
    constructor(private router :Router,private iab: InAppBrowser,private fDatabase :AngularFireDatabase ) {
        this.listlinks()
    }
 
   ngOnInit() {

    
   }

openurl(url) {
  this.iab.create(url, `_blank`);
}
listlinks(){
  this.fDatabase.list('/liens-utiles').valueChanges().subscribe(res=>{
      console.log(res)
      this.listL=res
  })}}
