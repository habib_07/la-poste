import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TelechargementsPage } from './telechargements.page';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import {AngularFireModule} from "angularfire2";
import {environment} from '../../../environments/environment';
import {AngularFireDatabaseModule} from "angularfire2/database";
import {AngularFireAuth} from "angularfire2/auth";



const routes: Routes = [
  {
    path: '',
    component: TelechargementsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
  ],
  providers:[
    InAppBrowser,
    AngularFireAuth,
  ],
  declarations: [TelechargementsPage]
})
export class TelechargementsPageModule {}
