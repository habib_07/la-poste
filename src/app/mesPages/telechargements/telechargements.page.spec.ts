import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TelechargementsPage } from './telechargements.page';

describe('TelechargementsPage', () => {
  let component: TelechargementsPage;
  let fixture: ComponentFixture<TelechargementsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TelechargementsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TelechargementsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
