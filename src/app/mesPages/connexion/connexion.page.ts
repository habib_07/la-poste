import { Component, OnInit } from '@angular/core';
import { Connexion, ConnexionServiceService } from 'src/app/mesServices/connexion-service.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AngularFireDatabase} from "angularfire2/database";
import {el} from '@angular/platform-browser/testing/src/browser_util';
import {Events} from '@ionic/angular';


@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.page.html',
  styleUrls: ['./connexion.page.scss']
 })
export class ConnexionPage implements OnInit {
    isActiveToggleTextPassword: Boolean = true;

  numCP:number
  mdp:string
    error

  tabs: Observable<Connexion[]>;

    constructor(private router :Router,private fDatabase :AngularFireDatabase, public events: Events,) {


  }
    loginforms=new FormGroup({
        ncpt:new FormControl('',Validators.compose([
            Validators.required,
            Validators.maxLength(11),Validators.minLength(11)
        ])),
        password:new FormControl(null,{
            validators:Validators.required,
            updateOn:'change'
        })
    })
 
   ngOnInit() {
    //this.tab = this.posteService.getCP();
   }
    public toggleTextPassword(): void{
        this.isActiveToggleTextPassword = (this.isActiveToggleTextPassword == true) ? false:true;
    }
    public getType() {
        return this.isActiveToggleTextPassword ? 'password' : 'text';
    }
    login(){
         this.numCP=this.loginforms.get('ncpt').value
         this.mdp=this.loginforms.get('password').value

        this.fDatabase.list('/clients/info-client',ref => ref.orderByChild("num_carte").equalTo(Number(this.numCP))).valueChanges().subscribe(res=>{
            console.log(res)
            if(res['length']===0){
                alert('no user found check ur numero de compt')

            }else {
                console.log(res[0]['mdp'])
                console.log(this.mdp)
                if(res[0]['mdp']==this.mdp){
                    this.router.navigate(['/home'])
                    localStorage.setItem('userkey',res[0]['num_carte'])
                    this.events.publish('user_key',res[0]['prenom'], Date.now());
                    this.events.publish('state','connected', Date.now());

                }else{
                    alert('check password')
                }
            }
        })
    }

   
  }
