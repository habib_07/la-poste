import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ConnexionPage } from './connexion.page';
import {AngularFireModule} from "angularfire2";
import {environment} from '../../../environments/environment';
import {AngularFireDatabaseModule} from "angularfire2/database";
import {AngularFireAuth} from "angularfire2/auth";

const routes: Routes = [
  {
    path: '',
    component: ConnexionPage
  }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        ReactiveFormsModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireDatabaseModule,
    ],
    providers:[
        AngularFireAuth,
    ],
  declarations: [ConnexionPage]
})
export class ConnexionPageModule {}
