import { Component, OnInit } from '@angular/core';
import {AngularFireDatabase} from "@angular/fire/database";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-details-bp',
  templateUrl: './details-bp.page.html',
  styleUrls: ['./details-bp.page.scss'],
})
export class DetailsBPPage implements OnInit {
idbp;
    listBP
  constructor(private fDatabase :AngularFireDatabase,private route:ActivatedRoute) {
  this.idbp=  this.route.snapshot.paramMap.get('id')
      console.log(this.idbp)
  }

  ngOnInit() {
    this.one_bp()
  }

    one_bp(){
    this.fDatabase.list('/bureaux-poste',ref => ref.orderByChild("id").equalTo(this.idbp)).valueChanges().subscribe(res=>{
            console.log(res[0])
        this.listBP=res[0]

        })
    }







}
