import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsBPPage } from './details-bp.page';

describe('DetailsBPPage', () => {
  let component: DetailsBPPage;
  let fixture: ComponentFixture<DetailsBPPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailsBPPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsBPPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
