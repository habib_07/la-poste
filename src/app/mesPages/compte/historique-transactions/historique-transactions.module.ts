import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { HistoriqueTransactionsPage } from './historique-transactions.page';

const routes: Routes = [
  {
    path: '',
    component: HistoriqueTransactionsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [HistoriqueTransactionsPage]
})
export class HistoriqueTransactionsPageModule {}
