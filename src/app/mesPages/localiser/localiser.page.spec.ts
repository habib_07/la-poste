import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocaliserPage } from './localiser.page';

describe('LocaliserPage', () => {
  let component: LocaliserPage;
  let fixture: ComponentFixture<LocaliserPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocaliserPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocaliserPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
