import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { LocaliserPage } from './localiser.page';
import {AgmCoreModule} from '@agm/core';
import {AngularFireModule} from "angularfire2";
import {environment} from '../../../environments/environment';
import {AngularFireDatabaseModule} from "angularfire2/database";
import {AngularFireAuth} from "angularfire2/auth";

const routes: Routes = [
  {
    path: '',
    component: LocaliserPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyBWnLEvgdiPhRHa-fmD6KIDO7PnaJY4cmY"
    }),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,

  ],
  providers:[
    AngularFireAuth,
  ],
  declarations: [LocaliserPage],
})
export class LocaliserPageModule {}
