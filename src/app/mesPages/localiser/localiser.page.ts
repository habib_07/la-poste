
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {
  GoogleMaps,
  GoogleMap,
  Geocoder,
   
  GeocoderResult,
  Marker
} from '@ionic-native/google-maps';
import { LoadingController, Platform } from '@ionic/angular';
import { LocaliserServiceService,BureauxDePoste } from 'src/app/mesServices/localiser-service.service';
import {Router} from '@angular/router';
import {AngularFireDatabase} from "angularfire2/database";


@Component({
  selector: 'app-localiser',
  templateUrl: './localiser.page.html',
  styleUrls: ['./localiser.page.scss'],
})
export class LocaliserPage implements OnInit {
  title: string = 'My first AGM project';
  lat: number ;
 listfinal
    listBP
    listDAB
    lng: number ;
  height = 0;
  url:string="../../../assets/local-svg.png"
    url_dab:string="../../../assets/icon.png"
    localisation:string="../../../assets/lo.png"
    searchbar=false
    onbp=false;
  ondab=false
    listtoshow=true
/*  @ViewChild('search_address') search_address: ElementRef;*/

  constructor(public platform: Platform,public router:Router,private fDatabase :AngularFireDatabase) {
      /*this.lat =36.742742299999996
      this.lng =10.3076411*/
    console.log(platform.height());
    this.height = platform.height() -56;
  }

   ngOnInit() {
    // Since ngOnInit() is executed before `deviceready` event,
    // you have to wait the event.
    this.getUserLocation()
       this.listbp()



  }


  getUserLocation() {
    console.log(navigator.geolocation)
    console.log("im in")
    /// locate the user

      navigator.geolocation.getCurrentPosition(position => {
        this.lat = position.coords.latitude;
        this.lng = position.coords.longitude;
        console.log('lat',this.lat)
        console.log('lang',this.lng)

      });

  }
  gotodetails(){
      this.router.navigate(['/details-bp'])
  }
    listBp:any=[
        {
            bureau:"KHZEMA EST",
            adresse:"Sousse",
            codep:"4051",
            ville:"BP",
           lat: 35.853771,
            lng:10.609441,

        },
        {
            bureau:"KALAA KBIRA",
            adresse:"Sousse",
            codep:"4071",
            ville:"BP",
           lat: 35.869708,
          lng:10.540674
        },
        {
            bureau:"H-SOUSSE",
            adresse:"hammam sousse",
            codep:"4083",
            ville:"BP",
            lat: 35.856714,
            lng:10.595510

        },
        {
            bureau:"Kalaa SGHIRA",
            adresse:"kalaa sghira sousse",
            codep:"4021",
            ville:"BP",
            lat:  35.827822,
            lng:10.559491
        },
        {
            bureau:"SAHLOUL",
            adresse:"sahloul sousse",
            codep:"4054",
            ville:"BP",
            lat: 35.840155,
            lng:10.597829
        },
        {
            bureau:"H-SOUSE GHRABI",
            adresse:"Sousse",
            codep:"4017",
            ville:"BP",
            lat: 35.854373,
            lng:10.587049
        },


    ]

    listbp(){
      this.fDatabase.list('/bureaux-poste').valueChanges().subscribe(res=>{

          this.listBP=res
          this.listfinal=this.listBP
          console.log('list',this.listfinal)
          this.listtoshow=true
      })

    }
    listdab(){
        this.fDatabase.list('/DAB').valueChanges().subscribe(res=>{
            console.log('listdab',res)
            this.listDAB=res
            this.listfinal=this.listDAB
            this.listtoshow=false
        })

    }
    showsr(){
      this.searchbar=true
    }
    hidesr(){
      this.searchbar=false
        this.ondab=false
        this.onbp=false

    }
    onBP(){
      this.onbp=true
        this.ondab=false
    }
    onDAB(){
        this.onbp=false
        this.ondab=true
    }
    search(){
      console.log('im in search')
      if(this.onbp==false&&this.ondab==true){
          this.listdab()
      }else{
          if(this.onbp==true&&this.ondab==false){
              this.listbp()
          }
      }
      this.hidesr()
    }



  
}
