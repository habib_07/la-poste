import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SoldesPage } from './soldes.page';

describe('SoldesPage', () => {
  let component: SoldesPage;
  let fixture: ComponentFixture<SoldesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SoldesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SoldesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
