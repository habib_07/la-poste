import { Component, OnInit } from '@angular/core';
import {AlertController, ToastController} from '@ionic/angular';
import {AngularFireDatabase} from "angularfire2/database";

@Component({
  selector: 'app-suivi',
  templateUrl: './suivi.page.html',
  styleUrls: ['./suivi.page.scss'],
})
export class SuiviPage implements OnInit {
  colie:any=[]

  constructor(public alertController: AlertController,private fDatabase :AngularFireDatabase,public toast:ToastController) {
    this.presentAlert()

  }

  ngOnInit() {

  }
  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Colis ',

      message: 'Saisissez le numéro de votre colis compose de 13 caractéres \n (Exemple de N°:EE995454657TN)',
      inputs: [
        {
          name: 'name1',
          type: 'text',
          placeholder: 'Numéro du colis'
        },

      ],
      buttons: [{
        text:"Chercher",
        handler:data=>{
          console.log("num colli",data['name1'])

          if(data['name1']==''){
            return this.presentToast("veuillez entrer votre numéro de colli") && false
          }
            else {
            this.collie_req(data['name1'])
              return true

          }
        }

      }]
    });

    await alert.present();
  }
  collie_req(code){
    this.fDatabase.list('/info-colis',ref => ref.orderByChild("code").equalTo(code)).valueChanges().subscribe(c=>{
      console.log(c)
      this.colie=c

      console.log('colie',this.colie)
      if(c.length==0){
        this.presentAlert()
        this.presentToast("aucune colis trouvé")
      }
    })


  }
  async presentToast(message) {
    console.log("im in toast")
    const toast = await this.toast.create({
      message:message,
      duration: 2000,
      color:"danger"
    });
    toast.present();
  }

}
