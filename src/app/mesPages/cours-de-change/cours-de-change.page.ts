import { Component, OnInit } from '@angular/core';
import {AngularFireDatabase} from '@angular/fire/database';

@Component({
  selector: 'app-cours-de-change',
  templateUrl: './cours-de-change.page.html',
  styleUrls: ['./cours-de-change.page.scss'],
})
export class CoursDeChangePage implements OnInit {
today=new Date()
  list_c_ch
  constructor(private fDatabase :AngularFireDatabase) {
  console.log(this.today)
  }

  ngOnInit() {
    this.fDatabase.list('/cours_change').valueChanges().subscribe(c_ch=>{
    console.log(c_ch)
      this.list_c_ch=c_ch

    })

  }


}
