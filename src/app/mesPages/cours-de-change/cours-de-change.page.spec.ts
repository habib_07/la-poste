import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoursDeChangePage } from './cours-de-change.page';

describe('CoursDeChangePage', () => {
  let component: CoursDeChangePage;
  let fixture: ComponentFixture<CoursDeChangePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoursDeChangePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursDeChangePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
