import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { firestore } from 'firebase';
import {AngularFireDatabase} from '@angular/fire/database';


@Component({
  selector: 'app-trouver-cp',
  templateUrl: './trouver-cp.page.html',
  styleUrls: ['./trouver-cp.page.scss'],
})
export class TrouverCPPage  {
    private BPList: any;
    private ref: any;
    private loadedBPList: any;
    term:string
    arraybp

  constructor( public router: Router,private fDatabase :AngularFireDatabase) {
this.listbp()
  }
listbp(){
        this.fDatabase.list('/bureaux-poste').valueChanges().subscribe(res=>{
            console.log(res)
            this.arraybp=res
        })
}

    listBP:any=[
        {
            bureau:"KHZEMA",
            adresse:"Sousse",
            codep:"4051",
            ville:"sousse"
        },
        {
            bureau:"KALAA KBIRA",
            adresse:"Sousse",
            codep:"4071",
            ville:"sousse"
        },
        {
            bureau:"H-SOUSSE",
            adresse:"hammam sousse",
            codep:"4083",
            ville:"sousse"
        },
        {
            bureau:"Ariana",
            adresse:"Av. Habib Bourguiba",
            codep:"2080",
            ville:"tunis"
        },
        {
            bureau:"Bir El Bey",
            adresse:"Av. de la Poste",
            codep:"2055",
            ville:"ben arous"
        },
        {
            bureau:"El Mrezgay",
            adresse:"El Mrezga",
            codep:"8058",
            ville:"nabeul"
        },


    ]
    gotodetails(id){
        this.router.navigate(['/details-bp',id])
    }

}




