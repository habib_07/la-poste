import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TrouverCPPage } from './trouver-cp.page';
import {AppModule} from '../../app.module';
import {BPSPipe} from '../../search/bps.pipe';
import {AngularFireModule} from "angularfire2";
import {environment} from '../../../environments/environment';
import {AngularFireDatabaseModule} from "angularfire2/database";
import {AngularFireAuth} from "angularfire2/auth";

const routes: Routes = [
  {
    path: '',
    component: TrouverCPPage
  }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireDatabaseModule,

    ],
    providers:[
        AngularFireAuth,
    ],
  declarations: [TrouverCPPage,BPSPipe],
    exports:[
        BPSPipe
    ]
})
export class TrouverCPPageModule {}
