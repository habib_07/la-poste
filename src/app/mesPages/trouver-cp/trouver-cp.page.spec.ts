import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrouverCPPage } from './trouver-cp.page';

describe('TrouverCPPage', () => {
  let component: TrouverCPPage;
  let fixture: ComponentFixture<TrouverCPPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrouverCPPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrouverCPPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
