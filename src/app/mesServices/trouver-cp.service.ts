import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestoreCollection, AngularFirestore, DocumentReference } from '@angular/fire/firestore';
import {map,take} from 'rxjs/operators'
import { promise } from 'protractor';


export interface BureauxDePoste{
  id?:string
  name:string
  codePostale:number
  governorate:string
  lat:number
  lng:number
}

@Injectable({
  providedIn: 'root'
})
export class TrouverCPService {
 


  private BP:Observable<BureauxDePoste[]>
  private BPCollection:AngularFirestoreCollection<BureauxDePoste>;

  constructor(private afs:AngularFirestore) {
    this.BPCollection=this.afs.collection<BureauxDePoste>('BureauxDePoste')
    this.BP=this.BPCollection.snapshotChanges().pipe(
      map(actions =>{
        return actions.map(res => {
          const data=res.payload.doc.data();
          const id= res.payload.doc.id;
          return{id, ...data};
        });
      })
    );
   }

   getPostes():Observable<BureauxDePoste[]>{
     return this.BP
   }
   getPoste(id:string):Observable<BureauxDePoste>{
    
    return this.BPCollection.doc<BureauxDePoste>(id).valueChanges().pipe(
      take(1),
      map(BP=> {
        BP.id=id;
        return BP;})
    

        
        )
      }
}

