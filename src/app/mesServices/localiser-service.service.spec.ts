import { TestBed } from '@angular/core/testing';

import { LocaliserServiceService } from './localiser-service.service';

describe('LocaliserServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LocaliserServiceService = TestBed.get(LocaliserServiceService);
    expect(service).toBeTruthy();
  });
});
