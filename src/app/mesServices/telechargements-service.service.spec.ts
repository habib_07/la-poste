import { TestBed } from '@angular/core/testing';

import { TelechargementsServiceService } from './telechargements-service.service';

describe('TelechargementsServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TelechargementsServiceService = TestBed.get(TelechargementsServiceService);
    expect(service).toBeTruthy();
  });
});
