import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestoreCollection, AngularFirestore, DocumentReference } from '@angular/fire/firestore';
import {map,take} from 'rxjs/operators'


export interface Connexion {
  id?:string
  numCP:string
  mdp:number

}


@Injectable({
  providedIn: 'root'
})
export class ConnexionServiceService {


  private CP:Observable<Connexion[]>
  private CPCollection:AngularFirestoreCollection<Connexion>;

  constructor(private afs:AngularFirestore) {

    this.CPCollection=this.afs.collection<Connexion>('connexion')
    this.CP=this.CPCollection.snapshotChanges().pipe(
      map(actions =>{
        return actions.map(res => {
          const data=res.payload.doc.data();
          const id= res.payload.doc.id;
          return{id, ...data};
        });
      })
    );
   }

   getCP():Observable<Connexion[]>{
    return this.CP
  }
}
