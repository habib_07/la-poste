import { TestBed } from '@angular/core/testing';

import { TrouverCPService } from './trouver-cp.service';

describe('TrouverCPService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TrouverCPService = TestBed.get(TrouverCPService);
    expect(service).toBeTruthy();
  });
});
