// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyBAJ2-1CmxLKeo1bgXvcBN0izhMpzNcbvs",
    authDomain: "la-poste-tunisienne.firebaseapp.com",
    databaseURL: "https://la-poste-tunisienne.firebaseio.com",
    projectId: "la-poste-tunisienne",
    storageBucket: "la-poste-tunisienne.appspot.com",
    messagingSenderId: "331558373021"
  },
  googleMapsKey: 'AIzaSyBOQZW32ho6VJMDZfUMj55DTp-G8RqW3v8\n' +
      'AIzaSyAMgfKUFl94IJH4bsF9E-icAPAtflWbeYY\n' +
      'AIzaSyD8sX1TF6aCgjcQu3TTmxLbCu4n2bI9NJs'
 /* googleMapsKey: 'AIzaSyAsFIvESiHyfocQHfP4_D84PU9nHYM9H14'*/
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
